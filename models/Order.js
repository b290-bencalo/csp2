const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, "User Id is required"],
  },
  firstName: {
    type: String,
    required: [true, "Name is required"],
  },
  products: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, "Product Id is required"],
      },
      name: {
        type: String,
        required: [true, "Product name is required"],
      },
      price: {
        type: Number,
        required: [true, "Price is required"],
      },      
      quantity: {
        type: Number,
        required: [true, "Quantity is required"],
      },
      subtotal: {
        type: Number,
        required: [true, "Subtotal is required"],
      }
    },
  ],
  totalAmount: {
    type: Number,
    required: [true, "Total Amount is required"],
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Order", orderSchema);
