// Necessary requirements

const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const cartRoute = require("./routes/cartRoute");
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const markDownRoute = require("./routes/markDownRoute");

const app = express();

// Connection
mongoose.connect(
  "mongodb+srv://admin:admin123@zuitt.sd0rdgy.mongodb.net/e-commerce?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.on(
  "error",
  console.error.bind(console, "connection error")
);
mongoose.connection.once("open", () =>
  console.log("Now connected to MongoDB Atlas.")
);

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use("/", markDownRoute);
app.use("/carts", cartRoute);
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

if (require.main === module) {
  app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
  });
}
module.exports = { app, mongoose };
