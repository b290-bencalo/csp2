const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");

// Create order
module.exports.createOrder = async (data) => {
  try {
    const product = await Product.findById(data.productId);
    if (product) {
      if (product.quantity >= data.quantity && product.isActive) {
        const newOrder = new Order({
          userId: data.userId,
          firstName: data.firstName,
          products: [
            {
              productId: data.productId,
              name: product.name,
              price: product.price,
              quantity: data.quantity,
              subtotal: product.price * data.quantity,
            },
          ],
          totalAmount: data.quantity * product.price,
        });
        product.quantity -= data.quantity;
        product.isActive = product.quantity >= 1;

        await product.save();

        const savedOrder = await newOrder.save();
        // console.log(savedOrder);
        return savedOrder;
      } else {
        // console.log("Quantity is not enough or product is not active");
        return false;
      }
    } else {
      // console.log("Product does not exist");
      return false;
    }
  } catch (err) {
    // console.log(err);
    return false;
  }
};

// Get user orders
module.exports.getUserOrders = (userId) => {
  return Order.find({ userId: userId })
    .then((order) => order)
    .catch((err) => {
      // console.log(err);
      return false;
    });
};

// Get all orders
module.exports.getAllOrders = () => {
  return Order.find({})
    .then((order) => order)
    .catch((err) => {
      // console.log(err);
      return false;
    });
};
