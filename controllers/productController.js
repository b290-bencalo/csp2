const Product = require("../models/Product");
const fs = require("fs");

// Create product
module.exports.createProduct = (reqBody) => {
  return Product.find({ name: reqBody.name }).then((result) => {
    if (result.length) {
      // console.log("Product already registered");
      return false;
    } else {
      let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        quantity: reqBody.quantity,
        isActive: reqBody.quantity >= 1,
      });
      return newProduct
        .save()
        .then((product) => {
          // console.log(product);
          return product;
        })
        .catch((err) => {
          // console.log(err);
          return false;
        });
    }
  });
};

// Show all products
module.exports.getAllProducts = () => {
  return Product.find({})
    .then((product) => product)
    .catch((err) => {
      // console.log(err);
      return false;
    });
};

// Show all active products
module.exports.getAllActiveProducts = () => {
  return Product.find({ isActive: true })
    .then((product) => product)
    .catch((err) => {
      // console.log(err);
      return false;
    });
};

// show specific product
module.exports.getProduct = (reqParams) => {
  return Product.findById(reqParams.productId)
    .then((product) => product)
    .catch((err) => {
      // console.log(err);
      return false;
    });
};

// Update product details
module.exports.updateProduct = (reqParams, reqBody) => {
  let updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
    quantity: reqBody.quantity,
    isActive: reqBody.quantity >= 1,
  };
  return Product.findOne({ name: reqBody.name }).then((result) => {
    if (result && result._id != reqParams.productId) {
      // console.log("Product already registered");
      return false;
    } else {
      return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
        .then((course) => true)
        .catch((err) => {
          // console.log(err);
          return false;
        });
    }
  });
};

// Archive product
module.exports.archiveProduct = (reqParams) => {
  return Product.findByIdAndUpdate(reqParams.productId, { isActive: false })
    .then((product) => true)
    .catch((err) => {
      // console.log(err);
      return false;
    });
};

// Activate product
module.exports.activateProduct = (reqParams) => {
  return Product.findByIdAndUpdate(reqParams.productId, { isActive: true })
    .then((product) => true)
    .catch((err) => {
      // console.log(err);
      return false;
    });
};
