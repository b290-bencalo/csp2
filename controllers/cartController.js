const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");

// Add to cart
let carts = {}; // Store carts in an object with user identifiers as keys
let orderId = 1;

module.exports.addToCart = async (data) => {
  try {
    const product = await Product.findOne({ _id: data.productId });

    if (product) {
      if (product.quantity >= data.quantity && product.isActive) {
        // Check if the user's cart exists
        if (!carts[data.id]) {
          // Create a new cart for the user
          carts[data.id] = {
            name: data.firstName,
            totalAmount: 0,
            products: [],
          };
        }

        const cart = carts[data.id];

        // Check if the product already exists in the cart
        const existingCartItem = cart.products.find(
          (item) => item.productId === data.productId
        );

        if (existingCartItem) {
          return false; // Product already in cart
        } else {
          let newCartItem = {
            orderId: orderId,
            productId: data.productId,
            name: product.name,
            quantity: data.quantity,
            price: product.price,
            subtotal: product.price * data.quantity,
          };
          cart.products.push(newCartItem);
        }

        cart.totalAmount = cart.products.reduce(
          (total, item) => total + item.subtotal,
          0
        );
        orderId++;

        return cart; // Return the updated cart
      } else {
        return false; // Quantity is not enough or product is not active
      }
    } else {
      return false; // Product does not exist
    }
  } catch (err) {
    // console.error("Error finding product:", err);
    return false;
  }
};

// Update quantity
module.exports.updateQuantity = async (reqBody, id) => {
  if (carts[id]) {
    let products = carts[id].products;
    let productFound = false;
    for (let i = 0; i < products.length; i++) {
      if (reqBody.productId == products[i].productId) {
        productFound = true;
        try {
          const product = await Product.findOne({ _id: reqBody.productId });
          if (
            product.quantity >= reqBody.quantity &&
            product.isActive &&
            reqBody.quantity > 0
          ) {
            products[i].quantity = reqBody.quantity;
            products[i].subtotal = reqBody.quantity * products[i].price;
            carts[id].totalAmount = carts[id].products.reduce(
              (total, item) => total + item.subtotal,
              0
            );
            return true;
          } else {
            // console.log("Quantity exceeds the remaining quantity");
            return false;
          }
        } catch (error) {
          // console.error("An error occurred while finding the product:", error);
          return false;
        }
      }
    }
    if (!productFound) {
      // console.log("Product not found");
      return false;
    }
  }
};

// Delete product cart
module.exports.deleteProductCart = (reqBody, id) => {
  return new Promise((resolve, reject) => {
    if (carts[id]) {
      let products = carts[id].products;
      let productFound = false;
      for (let i = 0; i < products.length; i++) {
        if (reqBody.productId == products[i].productId) {
          products.splice(i, 1);
          carts[id].totalAmount = carts[id].products.reduce(
            (total, item) => total + item.subtotal,
            0
          );
          productFound = true;
          resolve(true);
        }
      }
      if (!productFound) {
        // console.log("Product not found");
        resolve(false);
      }
    } else {
      resolve(false);
    }
  });
};

module.exports.showCart = (id) => {
  return new Promise((resolve, reject) => {
    if (carts[id]) {
      carts[id].totalAmount = carts[id].products.reduce(
        (total, item) => total + item.subtotal,
        0
      );
      resolve(carts[id]);
    } else {
      resolve(false); // Cart not found
    }
  }).catch((err) => {
    // console.log(err);
    return false;
  });
};

module.exports.orderCart = async (data) => {
  if (carts[data.userId]) {
    let products = carts[data.userId].products;
    const orderProducts = [];

    for (let i = 0; i < products.length; i++) {
      const product = await Product.findById(products[i].productId);
      const newOrderProduct = {
        productId: products[i].productId,
        name: products[i].name,
        price: products[i].price,
        quantity: products[i].quantity,
        subtotal: products[i].subtotal,
      };
      orderProducts.push(newOrderProduct);

      product.quantity -= products[i].quantity;
      product.isActive = product.quantity >= 1;

      await product.save();
    }

    const newOrder = new Order({
      userId: data.userId,
      firstName: data.firstName,
      products: orderProducts,
      totalAmount: carts[data.userId].totalAmount,
    });

    const savedOrder = await newOrder.save();
    carts = {};
    // console.log(savedOrder);
    return savedOrder;
  }
};
