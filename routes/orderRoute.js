const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController");

// Create Order
router.post("/checkout", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    firstName: auth.decode(req.headers.authorization).firstName,
    productId: req.body.id,
    quantity: req.body.quantity,
  };
  if (!isAdmin) {
    orderController
      .createOrder(data)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

// Show orders
router.get("/myOrders", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const userId = auth.decode(req.headers.authorization).id;
  if (!isAdmin) {
    orderController
      .getUserOrders(userId)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

// Show all orders
router.get("/", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    orderController
      .getAllOrders()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

module.exports = router;
