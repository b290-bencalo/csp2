const express = require("express");
const router = express.Router();
const auth = require("../auth");
const Product = require("../models/Product");
const productController = require("../controllers/productController");
const multer = require("multer");

// Create Product

router.post("/add", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    productController
      .createProduct(req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

// Show all products
router.get("/", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    productController
      .getAllProducts()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

// Show all active products
router.get("/active", (req, res) => {
  productController
    .getAllActiveProducts()
    .then((resultFromController) => res.send(resultFromController));
});

// Show specific product
router.get("/:productId", (req, res) => {
  productController
    .getProduct(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// Update Product details
router.put("/:productId/update", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    productController
      .updateProduct(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

// Archive product
router.patch("/:productId/archive", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    productController
      .archiveProduct(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

// Activate product
router.patch("/:productId/activate", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    productController
      .activateProduct(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

// Set up Multer storage
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

router.post("/:productId/image", upload.single("image"), async (req, res) => {
  try {
    const product = await Product.findById(req.params.productId);
    if (!req.file) {
      return res.status(400).send("No image file provided.");
    }
    product.image.data = req.file.buffer;
    product.image.contentType = req.file.mimetype;
    await product.save();
    res.send("Image uploaded successfully.");
  } catch (error) {
    // console.error("Error uploading image:", error);
    res.status(500).send("Error uploading image.");
  }
});

router.get("/:productId/image", async (req, res) => {
  try {
    const product = await Product.findById(req.params.productId);
    if (!product || !product.image.data) {
      return res.status(404).send("Image not found.");
    }
    res.set("Content-Type", product.image.contentType);
    res.send(product.image.data);
  } catch (error) {
    // console.error("Error retrieving image:", error);
    res.status(500).send("Error retrieving image.");
  }
});

module.exports = router;
