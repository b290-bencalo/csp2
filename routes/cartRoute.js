const express = require("express");
const router = express.Router();
const auth = require("../auth");
const cartController = require("../controllers/cartController");

// Add to cart
router.post("/addToCart", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const data = {
    id: auth.decode(req.headers.authorization).id,
    firstName: auth.decode(req.headers.authorization).firstName,
    productId: req.body.id,
    quantity: req.body.quantity,
  };
  if (!isAdmin) {
    cartController
      .addToCart(data)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

// Update Quantity
router.patch("/updateQuantity", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const id = auth.decode(req.headers.authorization).id;
  if (!isAdmin) {
    cartController
      .updateQuantity(req.body, id)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

// Delete product in cart
router.delete("/deleteProductCart", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const id = auth.decode(req.headers.authorization).id;
  if (!isAdmin) {
    cartController
      .deleteProductCart(req.body, id)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

router.get("/", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const id = auth.decode(req.headers.authorization).id;
  if (!isAdmin) {
    cartController
      .showCart(id)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});

router.post("/orderCart", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    firstName: auth.decode(req.headers.authorization).firstName,
  };

  if (!isAdmin) {
    cartController
      .orderCart(data)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    // console.log({ error: "Forbidden" });
    res.send(false);
  }
});
module.exports = router;
