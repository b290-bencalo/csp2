const express = require("express");
const fs = require("fs");
const markdownIt = require("markdown-it");
const router = express.Router();

const md = new markdownIt();

router.get("/", (req, res) => {
  fs.readFile("./guide.md", "utf-8", (err, data) => {
    if (err) {
      // console.error(err);
      return res.status(500).send("Error reading Markdown file");
    }

    // Convert Markdown to HTML
    const htmlContent = md.render(data);

    // Send the HTML response
    res.send(htmlContent);
  });
});

module.exports = router;
