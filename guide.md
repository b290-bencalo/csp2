# Guide (E-Commerce API using Postman)

---

1. **User Registration**

   > Register your first name, last name, email and password.

   > Duplicate email is not allowed.

2. **User Authentication**

   > Log in your registered email and password.

   > Wrong email or password is not allowed.

3. **Create Product (Admin Only)**

   > This can be done by admin only.

   > Add the name, description, price and quantity of the product.

   > Duplicate name is not allowed.

   > If quantity is 0, the product will become inactive.

4. **All Products (Admin Only)**

   > This can be done by admin only.

   > All products will be shown, active or inactive.

5. **All Active Products**

   > Even not registered users can access this.

   > Shows all active products.

6. **Single Product**

   > Even not registered users can access this.

   > Show a single product of choice.

7. **Update Product (Admin Only)**

   > This can be done by admin only.

   > Restricts changing the name to a name that is already registered.

   > If the quantity is updated to 0, the product will become inactive.

8. **Archive Product (Admin Only)**

   > This can be done by admin only.

   > The product will become inactive.

9. **Activate Product (Admin Only)**

   > This can be done by admin only.

   > This product will become active.

10. **Checkout (Non-Admin Create Order)**

    > This can be done by non-admin only.

    > Input the id and quantity of the product chosen.

    > If the product is inactive, the order won't push through.

    > If the quantity inputted is more than the remaining quantity of the product, the order won't push through.

    > After the order, the quantity of the product will be deducted.

    > If the quantity of the product becomes 0, the product will be inactive.

11. **Retrieve User Details**

    > Shows user details of the user logged in.

12. **Set User as Admin (Admin Only)**

    > This can be done by admin only.

    > Set any user to admin.

13. **Remove Admin as Admin (Admin Only)**

    > This can be done by admin only.

    > Remove any admin as admin.

14. **My Orders**

    > This can be done by non-admin only.

    > Show all the user's orders from the beginning.

15. **Retrieve All Orders (Admin Only)**

    > This can be done by admin only.

    > Show all the orders.

16. **Add to cart**

    > This can be done by non-admin only.

    > Add any items into cart.

    > Input the id of the product and quantity.

    > If the quantity is more than the remaining quantity of the product, the add to cart won't push through.

    > If the product is inactive, the add to cart won't push through.

    > The total amount will be updated after adding products to the cart.

17. **Update quantity of products in cart**

    > This can be done by non-admin only.

    > Update the quantity of the product in cart by inputting order ID and updating the quantity.

    > If the quantity is more than the remaining quantity of the product, the update quantity won't push through.

18. **Delete product in cart**

    > This can be done by non-admin only.

    > Delete a product in the cart by inputting the order ID.

19. **Show cart**

    > This can be done by non-admin only.

    > Show all the products in the cart.

20. **Checkout order in cart**

    > This can be done by non-admin only.

    > All the products in the cart will be ordered.

    > After the order, the quantity of the product will be deducted.
